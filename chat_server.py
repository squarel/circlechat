""" This module is CircleChat chat server. """

from twisted.internet.protocol import Factory, Protocol
from twisted.internet import reactor
import simplejson as json

from utils import datetime_now


class CircleChat(Protocol):
    def connectionMade(self):
        #self.factory.all_clients.append(self)
        #print "clients are ", self.factory.all_clients
        print "new client:", self

    def connectionLost(self, reason):
        self.factory.clients[self.room_id].remove(self)
        for cl in self.factory.clients[self.room_id]:
            cl.message_leave(self.name)

    def dataReceived(self, data):
        try:
            json_obj = json.loads(data)
        except json.scanner.JSONDecodeError:
            print "can not decode json"
            return

        if 'room' in json_obj:
            print json_obj
            self.room_id = json_obj['room']
            if self.room_id not in self.factory.clients:
                self.factory.clients[self.room_id] = []
            self.factory.clients[self.room_id].append(self)
            self.name = json_obj['name']
            print self.factory.clients

            for cl in self.factory.clients[self.room_id]:
                cl.message_join(self.name)

        elif 'msg' in json_obj:
            for cl in self.factory.clients[self.room_id]:
                cl.message(json_obj['msg'], self.name)

    def message_join(self, name):
        """ send join message . """
        json_obj = {'iam': name,
                    'time': datetime_now()}
        msg = json.dumps(json_obj)
        print "join", msg
        self.transport.write(msg + '\n')

    def message(self, msg, name):
        """ send general message. """
        json_obj = {'msg': msg,
                    'name': name,
                    'time': datetime_now()}
        msg = json.dumps(json_obj)
        print "msg", msg
        self.transport.write(msg + '\n')

    def message_leave(self, name):
        """ send leaving message. """
        json_obj = {'out': name,
                    'time': datetime_now()}
        msg = json.dumps(json_obj)
        print "leave", msg
        self.transport.write(msg + '\n')



def main():
    factory = Factory()
    factory.protocol = CircleChat
    factory.all_clients = []
    factory.clients = {}
    reactor.listenTCP(80, factory)
    print "CircleChat chat server started"
    reactor.run()

if __name__ == "__main__":
    main()
