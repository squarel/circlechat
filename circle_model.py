""" This file is the ORM of CircleChat. """

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, Text, Column, Integer, String, Float,\
    DateTime
from sqlalchemy.orm import sessionmaker

from utils import earth_distance

Base = declarative_base()
engine = create_engine('sqlite:///:memory')
Session = sessionmaker(bind=engine)

#######
# Room


class CircleRoom(Base):

    """ Room Table, has the location info of certain chatting room. """

    __tablename__ = 'circle_room'
    id = Column('id', Integer, primary_key=True)
    name = Column('name', String(256), nullable=False, unique=True)
    lat = Column('lat', Float, nullable=False)
    lng = Column('lng', Float, nullable=False)
    radius = Column('radius', Integer, nullable=False)
    comment = Column('comment', Text, nullable=True)
    created_by = Column('created_by', String(256), nullable=False)
    created_at = Column('created_at', DateTime, nullable=True)

    def __repr__(self):
        return "<Room(id='%s, name='%s', latlng='%s,%s', radius='%s',\
                created_by='%s')" % (self.id, self.name, self.lat, self.lng,
                                     self.radius, self.created_by)

    def intersect(self, lat, lng, radius):
        """ Check if given circle intersect with this room, return boolean."""
        dist = earth_distance(lat, lng, self.lat, self.lng)
        if (radius + self.radius) < dist:
            return False
        else:
            return True

    def contain(self, lat, lng):
        """ Check if given coordinate is in the room circle, return boolean."""
        dist = earth_distance(lat, lng, self.lat, self.lng)
        if self.radius < dist:
            return False
        else:
            return True


def main():
    """ Create schemas."""

    Base.metadata.create_all(engine)

if __name__ == "__main__":
    main()
