""" Load mock data of rooms to database."""

from circle_model import CircleRoom, Session


def main():
    """ main."""

    session = Session()
    for room in session.query(CircleRoom).all():
        session.delete(room)
    session.add(CircleRoom(name="eaves", lat=42.2738, lng=-71.031233,
                           radius=50, created_by="mock"))
    session.add(CircleRoom(name="saga", lat=42.27335, lng=-71.032533,
                           radius=50, created_by="mock"))
    session.commit()
    print session.query(CircleRoom).all()


if __name__ == "__main__":
    main()
