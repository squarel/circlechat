"""
This module is CircleChat room list server.
GET request to server with latitude, longitude, radius as arguments,
return chatting rooms available in that range.
"""

from twisted.web import server, resource
from twisted.internet import reactor
import simplejson as json

from circle_model import CircleRoom, Session


class RoomResource(resource.Resource):
    isLeaf = True

    def __init__(self, session):
        resource.Resource.__init__(self)
        self.session = session

    def _get_rooms(self, lat, lng, radius):
        """
        Given a coordinate with radius,
        return available rooms in that range.
        """
        rooms = self.session.query(CircleRoom).all()
        available_rooms = []
        for room in rooms:
            if room.intersect(lat, lng, radius):
                available_rooms.append(room)
        return available_rooms

    def _output_json(self, rooms):
        """ Given a list of rooms, return json format."""
        json_objs = []
        if not rooms:
            return json_objs
        for room in rooms:
            room_dict = {}
            room_dict['title'] = room.name
            room_dict['lat'] = room.lat
            room_dict['lng'] = room.lng
            room_dict['radius'] = room.radius
            room_dict['room_id'] = room.id
            room_dict['created_by'] = room.created_by
            json_objs.append(room_dict)
        return json_objs

    def render_GET(self, request):
        request.setHeader("content-type", "text/plain")
        if 'lat' in request.args and\
           'lng' in request.args and\
           'radius' in request.args:
            radius = int(request.args['radius'][0])
            lat = float(request.args['lat'][0])
            lng = float(request.args['lng'][0])
        else:
            return json.dumps([]) + "\n"
        avail_rooms = self._get_rooms(lat, lng, radius)
        json_obj = self._output_json(avail_rooms)

        return json.dumps(json_obj) + "\n"

    def render_POST(self, request):
        request.setHeader("content-type", "text/plain")
        if 'lat' in request.args and\
           'lng' in request.args and\
           'user' in request.args and\
           'title' in request.args and\
           'radius' in request.args:
            radius = int(request.args['radius'][0])
            lat = float(request.args['lat'][0])
            lng = float(request.args['lng'][0])
            user = request.args['user'][0]
            title = request.args['title'][0]
            new_room = CircleRoom(name=title, lat=lat, lng=lng, radius=radius,
                                  created_by=user)
            self.session.add(new_room)
            self.session.commit()
            print new_room
            print new_room.id
            return new_room.id
        return "-1"


def main():
    session = Session()
    reactor.listenTCP(8080, server.Site(RoomResource(session)))
    reactor.run()

if __name__ == "__main__":
    main()
