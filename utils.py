""" Utils for CircleChat."""

import math
import datetime


def earth_distance(lat1, lng1, lat2, lng2):
    """ given two decimal coordinates on earth return the distance in meter."""

    radius = 6371000  # meter
    dlat = math.radians(math.fabs(lat2 - lat1))
    dlon = math.radians(math.fabs(lng2 - lng1))
    a = math.sin(dlat / 2) * math.sin(dlat / 2) +\
        math.cos(math.radians(math.fabs(lat1))) *\
        math.cos(math.radians(math.fabs(lat2))) *\
        math.sin(dlon / 2) * math.sin(dlon / 2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    d = radius * c
    return int(d)


def datetime_now():
    """ Return general datetime format."""
    return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")


def main():
    """ Test some functions. """

    print "distance between eaves and sagamore is: %sm" %\
        earth_distance(42.2738, -71.031233, 42.27335, -71.032533)
    print "between pin and eaves: %sm" %\
        earth_distance(42.273967, -71.032283, 42.2738, -71.031233)
    print "between pin and sagamore: %sm" %\
        earth_distance(42.273967, -71.032283, 42.27335, -71.032533)

if __name__ == "__main__":
    main()
